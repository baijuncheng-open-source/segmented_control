package info.hoang8f.ohos.segmented.slice;

import info.hoang8f.ohos.library.SegmentedGroup;
import info.hoang8f.ohos.segmented.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.RadioContainer;
import ohos.agp.components.RadioButton;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.window.dialog.ToastDialog;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button addBtn = (Button) findComponentById(ResourceTable.Id_add_segmented);
        Button removeBtn = (Button) findComponentById(ResourceTable.Id_remove_segmented);
        segmented5 = (SegmentedGroup) findComponentById(ResourceTable.Id_segmented5);
        addBtn.setClickedListener(this);
        removeBtn.setClickedListener(this);
        segmented5.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int i) {
                new ToastDialog(getContext()).setText("radio: " + (i + 1) + " checked").show();
            }
        });

    }

    SegmentedGroup segmented5;

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_add_segmented:
                addButton(segmented5);
                break;
            case ResourceTable.Id_remove_segmented:
                removeButton(segmented5);
                break;
            default:
                // Nothing to do
        }

    }


    private void addButton(SegmentedGroup group) {
        RadioButton radioButton = (RadioButton) LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_radio_button_item, null, false);
        radioButton.setText("Button " + (group.getChildCount() + 1));
        radioButton.setBubbleSize(0, 0);
        group.addComponent(radioButton);
        group.updateBackground();
    }

    private void removeButton(SegmentedGroup group) {
        if (group.getChildCount() < 1) return;
        group.removeComponentAt(group.getChildCount() - 1);
        group.updateBackground();

        //Update margin for last item
        if (group.getChildCount() < 1) return;
        ComponentContainer.LayoutConfig layoutParams = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        layoutParams.setMargins(0, 0, 0, 0);
        group.getComponentAt(group.getChildCount() - 1).setLayoutConfig(layoutParams);
    }

}
