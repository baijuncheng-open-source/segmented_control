package info.hoang8f.ohos.library;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.RadioContainer;
import ohos.agp.components.Component;
import ohos.agp.components.AttrSet;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.RadioButton;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.components.ComponentState;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.multimodalinput.event.TouchEvent;

public class SegmentedGroup extends RadioContainer implements Component.BindStateChangedListener, Component.TouchEventListener {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "SegmentedGroup");
    private int mMarginDp;
    private int mTintColor;
    private int mUnCheckedTintColor;
    private int mCheckedTextColor = Color.WHITE.getValue();
    private LayoutSelector mLayoutSelector;
    private int mCornerRadius;
    private RadioContainer.CheckedStateChangedListener mCheckedChangeListener;
    private int mLastCheckId;

    public SegmentedGroup(Context context) {
        super(context);
    }

    public SegmentedGroup(Context context, AttrSet attrs) {
        super(context, attrs);
        mTintColor = ResourceTable.Color_radio_button_selected_color;
        mUnCheckedTintColor = ResourceTable.Color_radio_button_unselected_color;
        mMarginDp = AttrHelper.vp2px(1, getContext());
        mCornerRadius = AttrHelper.vp2px(5, getContext());

        initAttrs(attrs);
        mLayoutSelector = new LayoutSelector(mCornerRadius);
        setBindStateChangedListener(this);

    }

    /* Reads the attributes from the layout */
    private void initAttrs(AttrSet attrs) {
        if (attrs.getAttr("sc_border_width").isPresent()) {
            mMarginDp = attrs.getAttr("sc_border_width").get().getDimensionValue();
        }

        if (attrs.getAttr("sc_corner_radius").isPresent()) {
            mCornerRadius = attrs.getAttr("sc_corner_radius").get().getDimensionValue();
        }
        if (attrs.getAttr("sc_tint_color").isPresent()) {
            mTintColor = attrs.getAttr("sc_tint_color").get().getColorValue().getValue();
        }
        if (attrs.getAttr("sc_checked_text_color").isPresent()) {
            mCheckedTextColor = attrs.getAttr("sc_checked_text_color").get().getColorValue().getValue();
        }
        if (attrs.getAttr("sc_unchecked_tint_color").isPresent()) {
            mUnCheckedTintColor = attrs.getAttr("sc_unchecked_tint_color").get().getColorValue().getValue();
        }
        updateBackground();
    }


    public void setTintColor(int tintColor) {
        mTintColor = tintColor;
        updateBackground();
    }

    public void setTintColor(int tintColor, int checkedTextColor) {
        mTintColor = tintColor;
        mCheckedTextColor = checkedTextColor;
        updateBackground();
    }

    public void setUnCheckedTintColor(int unCheckedTintColor, int unCheckedTextColor) {
        mUnCheckedTintColor = unCheckedTintColor;
        updateBackground();
    }

    public void updateBackground() {
        int count = super.getChildCount();
        for (int i = 0; i < count; i++) {
            Component child = getComponentAt(i);
            updateBackground((RadioButton) child);
            child.setClickable(false);
            addClick(child);
            // If this is the last view, don't set LayoutParams
            if (i == count - 1) break;

            ComponentContainer.LayoutConfig initParams = (ComponentContainer.LayoutConfig) child.getLayoutConfig();
//            ComponentContainer.LayoutConfig params = new ComponentContainer.LayoutConfig(initParams.width, initParams.height);
            // Check orientation for proper margins
            if (getOrientation() == DirectionalLayout.HORIZONTAL) {
                initParams.setMargins(0, 0, -mMarginDp, 0);
            } else {
                initParams.setMargins(0, 0, 0, -mMarginDp);
            }
            child.setLayoutConfig(initParams);
        }
    }

    private void addClick(Component child) {
        child.setTouchEventListener(this);
    }

    private void updateBackground(RadioButton view) {
        view.setTouchEventListener(new TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                return true;
            }
        });
        int checked = mLayoutSelector.getSelected();
        int unchecked = mLayoutSelector.getUnselected();

        view.setTextColorOff(new Color(mTintColor));
        view.setTextColorOn(new Color(mCheckedTextColor));


        ShapeElement checkedDrawable = new ShapeElement();
        ShapeElement uncheckedDrawable = new ShapeElement();
        checkedDrawable.setShape(ShapeElement.RECTANGLE);
        uncheckedDrawable.setShape(ShapeElement.RECTANGLE);

        checkedDrawable.setRgbColor(RgbColor.fromArgbInt(mTintColor));
        uncheckedDrawable.setRgbColor(RgbColor.fromArgbInt(mUnCheckedTintColor));

        checkedDrawable.setStroke(mMarginDp, RgbColor.fromArgbInt(mTintColor));
        uncheckedDrawable.setStroke(mMarginDp, RgbColor.fromArgbInt(mTintColor));
        checkedDrawable.setCornerRadiiArray(mLayoutSelector.getChildRadii(view));
        uncheckedDrawable.setCornerRadiiArray(mLayoutSelector.getChildRadii(view));
        view.setButtonElement(null);
        if (view.getTag() == null) {
            view.setText(view.getText() + "  "); //alignment center
            view.setTag(true);
        }

        StateElement checkElement = new StateElement();
        checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, checkedDrawable);
        checkElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, uncheckedDrawable);
        view.setBackground(checkElement);


    }


    @Override
    public void setMarkChangedListener(CheckedStateChangedListener listener) {
        mCheckedChangeListener = listener;
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        HiLog.warn(LABEL, "onComponentBoundToWindow: counter %{public}d", 0);
        updateBackground();
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {

    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        int action = touchEvent.getAction();
        if (action == TouchEvent.PRIMARY_POINT_UP) {
            boolean currentState = ((RadioButton) component).isChecked();
            if (currentState) {
                return true;
            } else {
                for (int i = 0; i < getChildCount(); i++) {
                    RadioButton radioButton = (RadioButton) getComponentAt(i);
                    radioButton.setChecked(radioButton == component ? true : false);
                    if (mCheckedChangeListener != null && radioButton == component) {
                        mCheckedChangeListener.onCheckedChanged(this, i);
                    }
                }
            }
        }
        return true;
    }


    /*
     * This class is used to provide the proper layout based on the view.
     * Also provides the proper radius for corners.
     * The layout is the same for each selected left/top middle or right/bottom button.
     * float tables for setting the radius via Gradient.setCornerRadii are used instead
     * of multiple xml drawables.
     */
    private class LayoutSelector {

        private int children;
        private int child;
        private final int SELECTED_LAYOUT = ResourceTable.Graphic_radio_checked;
        private final int UNSELECTED_LAYOUT = ResourceTable.Graphic_radio_unchecked;

        private float r;    //this is the radios read by attributes or xml dimens
        private final float r1 = AttrHelper.vp2px(0.1f, getContext());
        ;    //0.1 dp to px

        private final float[] rLeft;    // left radio button
        private final float[] rRight;   // right radio button
        private final float[] rMiddle;  // middle radio button
        private final float[] rDefault; // default radio button
        private final float[] rTop;     // top radio button
        private final float[] rBot;     // bot radio button
        private float[] radii;          // result radii float table

        public LayoutSelector(float cornerRadius) {
            children = -1; // Init this to force setChildRadii() to enter for the first time.
            child = -1; // Init this to force setChildRadii() to enter for the first time
            r = cornerRadius;
            rLeft = new float[]{r, r, r1, r1, r1, r1, r, r};
            rRight = new float[]{r1, r1, r, r, r, r, r1, r1};
            rMiddle = new float[]{r1, r1, r1, r1, r1, r1, r1, r1};
            rDefault = new float[]{r, r, r, r, r, r, r, r};
            rTop = new float[]{r, r, r, r, r1, r1, r1, r1};
            rBot = new float[]{r1, r1, r1, r1, r, r, r, r};
        }

        private int getChildren() {
            return SegmentedGroup.this.getChildCount();
        }

        private int getChildIndex(Component view) {
            return SegmentedGroup.this.getChildIndex(view);
        }

        private void setChildRadii(int newChildren, int newChild) {

            // If same values are passed, just return. No need to update anything
            if (children == newChildren && child == newChild)
                return;

            // Set the new values
            children = newChildren;
            child = newChild;

            // if there is only one child provide the default radio button
            if (children == 1) {
                radii = rDefault;
            } else if (child == 0) { //left or top
                radii = (getOrientation() == DirectionalLayout.HORIZONTAL) ? rLeft : rTop;
            } else if (child == children - 1) {  //right or bottom
                radii = (getOrientation() == DirectionalLayout.HORIZONTAL) ? rRight : rBot;
            } else {  //middle
                radii = rMiddle;
            }
        }

        /* Returns the selected layout id based on view */
        public int getSelected() {
            return SELECTED_LAYOUT;
        }

        /* Returns the unselected layout id based on view */
        public int getUnselected() {
            return UNSELECTED_LAYOUT;
        }

        /* Returns the radii float table based on view for Gradient.setRadii()*/
        public float[] getChildRadii(Component view) {
            int newChildren = getChildren();
            int newChild = getChildIndex(view);
            setChildRadii(newChildren, newChild);
            return radii;
        }
    }
}