package info.hoang8f.ohos.library.font;


import info.hoang8f.ohos.library.utils.TypefaceProvider;
import ohos.agp.render.Paint;
import ohos.app.Context;

/**
 * A custom span which paints text using the typeface specified by the IconSet passed to the constructor
 */
public class AwesomeTypefaceSpan {

    private final Context context;
    private final IconSet iconSet;

    public AwesomeTypefaceSpan(Context context, IconSet iconSet) {
//        super(iconSet.fontPath().toString());
        this.context = context.getApplicationContext();
        this.iconSet = iconSet;
    }


    public void updateDrawState(Paint ds) {
        ds.setFont(TypefaceProvider.getTypeface(context, iconSet));
    }

    public void updateMeasureState(Paint paint) {
        paint.setFont(TypefaceProvider.getTypeface(context, iconSet));
    }

}
