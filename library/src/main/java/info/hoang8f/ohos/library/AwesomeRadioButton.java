package info.hoang8f.ohos.library;


import ohos.agp.components.AttrSet;
import ohos.agp.components.RadioButton;
import ohos.app.Context;

public class AwesomeRadioButton extends RadioButton {

    private String bootstrapText;

    public AwesomeRadioButton(Context context) {
        super(context);
    }

    public AwesomeRadioButton(Context context, AttrSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public AwesomeRadioButton(Context context, AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        init(attrs);
    }

    private void init(AttrSet attrs) {
        String markdownText = "";
        if (attrs.getAttr("awesome_text").isPresent()) {
            markdownText = attrs.getAttr("awesome_text").get().getStringValue();
        }

        if (markdownText != null) {
            setMarkdownText(markdownText);
        }
        updateBootstrapState();
    }


    public void setBootstrapText(String bootstrapText) {
        this.bootstrapText = bootstrapText;
        updateBootstrapState();
    }


    public String getBootstrapText() {
        return bootstrapText;
    }


    public void setMarkdownText(String text) {
        String textSpace = text;
        setBootstrapText(text);
    }

    protected void updateBootstrapState() {
        if (bootstrapText != null) {
            setText(bootstrapText);
        }
    }
}
