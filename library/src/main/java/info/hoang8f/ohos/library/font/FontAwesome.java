package info.hoang8f.ohos.library.font;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.HashMap;
import java.util.Map;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;


public class FontAwesome implements IconSet {

    public static final String FONT_PATH = "fontawesome-webfont.ttf";

    @Override
    public CharSequence unicodeForKey(CharSequence key) {
        return ICON_MAP.get(key.toString());
    }

    @Override
    public CharSequence iconCodeForAttrIndex(int index) {
        return ATTR_MAP.get(index);
    }

    @Override
    public CharSequence fontPath() {
        return FONT_PATH;
    }

    // Auto-generated Icon Set from 2015-10-05

    private static final Map<String, String> ICON_MAP = new HashMap<>();
    private static final Map<Integer, String> ATTR_MAP = new HashMap<>();




    @Retention(SOURCE)
    @Target({ANNOTATION_TYPE})
    public @interface StringDef {
        /**
         * Defines the allowed constants for this element
         */
        String[] value() default {};
    }

}
